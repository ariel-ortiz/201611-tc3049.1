#==========================================================
# Type your name and student ID here.
#==========================================================

#==========================================================
# Code for question 2

#----------------------------------------------------------
# Place here your code for the Twitter class.
#----------------------------------------------------------

require 'observer'

class Twitter

  include Observable

  def initialize(name)
    @name = name
  end

  def follow(twitter)
    twitter.add_observer(self, :tell)
    self
  end

  def tweet(message)
    puts "#{ @name } just tweeted: #{ message }"
    changed
    notify_observers(@name, message)
  end

  def tell(who_tweets, message)
    puts "#{ @name } received a tweet from #{ who_tweets }: #{ message }"
  end

end
