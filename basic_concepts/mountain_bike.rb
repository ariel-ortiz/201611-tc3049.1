# This class represents a mountain bike which allows
# us to calculate it's total price.
class MountainBike

  # Value-added tax.
  TAX = 0.16

  # Initialize the base price of the mountain bike.
  #
  # Parameter::
  #
  #     base_price:: The mountain bike base price.
  def initialize(base_price)
    @base_price = base_price
  end

  # Computes the price of a mountain bike taking into account
  # its base price, the value-added tax, and if it has
  # front/rear suspensions.
  #
  # Returns:: The full price of the mountain bike.
  def calc_price
    (1 + TAX) * @base_price
  end
end

# This class represents a rigid mountain bike which allows
# us to calculate it's total price.
class RigidMountainBike < MountainBike
end

# This class represents a mountain bike with front suspension
# which allows us to calculate it's total price.
class FrontSuspensionMountainBike < MountainBike

  # The price of the front suspension.
  FRONT_SUSPENSION_PRICE = 30

  # Computes the price of a mountain bike taking into account
  # its base price, the value-added tax, and if it has
  # front/rear suspensions.
  #
  # Returns:: The full price of the mountain bike.
  def calc_price
    super + FRONT_SUSPENSION_PRICE
  end
end

# This class represents a mountain bike with front and rear
# suspension which allows us to calculate it's total price.
class FullSuspensionMountainBike < MountainBike

  # The price of the rear suspension.
  REAR_SUSPENSION_PRICE = 60

  # Computes the price of a mountain bike taking into account
  # its base price, the value-added tax, and if it has
  # front/rear suspensions.
  #
  # Returns:: The full price of the mountain bike.
  def calc_price
    super + FrontSuspensionMountainBike::FRONT_SUSPENSION_PRICE +
      REAR_SUSPENSION_PRICE
  end
end
