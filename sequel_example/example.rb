require 'sequel'

DB = Sequel.connect('sqlite://students.db')

require ('./models/student')

students = DB[:students]

students.where(id: 3).update(name: 'María')

students.where(name: 'John').delete

students.reverse_order(:gpa).each do |row|
  puts "ID = #{ row[:id] }, Name = #{ row[:name] }, GPA = #{ row[:gpa] }"
end

puts "Largest GPA = #{ students.max(:gpa) }"
puts "GPA Average = #{ students.avg(:gpa) }"

