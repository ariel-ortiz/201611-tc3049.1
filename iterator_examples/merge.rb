require 'enumerator_adapter'

def merge(array1, array2)
  merged = []

  iterator1 = EnumeratorAdapter.new(array1.each)
  iterator2 = EnumeratorAdapter.new(array2.each)

  while( iterator1.has_next? and iterator2.has_next? )
    if iterator1.item < iterator2.item
      merged << iterator1.next_item
    else
      merged << iterator2.next_item
    end
  end

  # Pick up the leftovers from array1

  while( iterator1.has_next?)
    merged << iterator1.next_item
  end

  # Pick up the leftovers from array2

  while( iterator2.has_next?)
    merged << iterator2.next_item
  end

  merged
end
