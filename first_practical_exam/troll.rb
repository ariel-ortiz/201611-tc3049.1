#==========================================================
# Type your name and student ID here.
#==========================================================

#==========================================================
# Code for question 1

class Troll
  attr_reader :name, :power
  def initialize(name, power)
    @name = name
    @power = power
  end
  def to_s
    "#{ @name } (#{ @power})"
  end
end

#----------------------------------------------------------
# Place here your code for these classes: TrollArmy,
# NameFormationStrategy, and PowerFormationStrategy.
#----------------------------------------------------------

class TrollArmy

  attr_accessor :formation_strategy

  def initialize
    @trolls = []
    @formation_strategy = nil
  end

  def add(t)
    @trolls << t
    self
  end

  def attack
    if formation_strategy.nil?
      puts 'Undefined strategy, cannot attack.'
    else
      puts '*** Attack formation ***'
      formation_strategy.arrange(@trolls).each do |t|
        puts t
      end
    end
  end

end

class NameFormationStrategy
  def arrange(trolls)
    trolls.sort_by {|t| t.name }
  end
end

class PowerFormationStrategy
  def arrange(trolls)
    trolls.sort_by {|t| -t.power }
  end
end
