require 'minitest/autorun' # Import unit test framework
require 'mountain_bike'    # Import file to be tested

class MountainBikeTest < Minitest::Test

  def test_mountain_bike_price
    b1 = RigidMountainBike.new(100)
    b2 = FrontSuspensionMountainBike.new(100)
    b3 = FullSuspensionMountainBike.new(100)
    assert_in_delta 116, b1.calc_price
    assert_in_delta 146, b2.calc_price
    assert_in_delta 206, b3.calc_price

    # The following code demonstrates the use of polymorphism.
    a = [b1, b2, b3]
    assert_in_delta 468, (a.map {|x| x.calc_price}).reduce(:+)
  end

end