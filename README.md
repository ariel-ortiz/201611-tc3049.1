# Tc3049.1 Software Design and Architecture

This project contains all the source files that the course instructor will be writing in class during the **2016 January-May semester**.

You can inspect the files directly through this site, or you can use git to obtain a local copy of these. In the latter case, you must do the following:

 1. If necessary, [install a git client](http://git-scm.com/downloads) in your computer. The [Cloud 9](http://c9.io/) platform has git already installed.
 
 2. Clone this repository. Type at the terminal:
    
        git clone https://bitbucket.org/ariel-ortiz/201611-tc3049.1.git tc3049
    
 3. Change your current working directory to `tc3049`:
    
        cd tc3049
    
    You will find all the project source files under this directory.

 4. Every time the instructor updates an existing file or creates a new one, you will need to run the repository *pull* command. Type at the terminal from within the `tc3049` directory:
    
        git pull
