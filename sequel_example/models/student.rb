DB.create_table? :students do
  primary_key :id
  String      :name
  Float       :gpa
end

if DB[:students].count == 0
  s = DB[:students]
  s.insert(name: 'John', gpa: 3.7)
  s.insert(name: 'Jane', gpa: 2.5)
  s.insert(name: 'Mary', gpa: 4.0)
end
