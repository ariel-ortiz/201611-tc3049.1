#!/usr/bin/env ruby

# Run your program like this:
#
#         ruby -I . -w hello.rb

class Example
  def initialize
    @name = 'Example'
  end
  def something
    p @name
  end
end

puts 'Hello world!'
e = Example.new
e.something