class EnumeratorAdapter

  def initialize(enum)
    @enum = enum
  end

  def has_next?
    begin
      @enum.peek
      true
    rescue StopIteration
      false
    end
  end

  def item
    has_next? ? @enum.peek : nil
  end

  def next_item
    has_next? ? @enum.next : nil
  end

end